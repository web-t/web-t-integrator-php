<?php
/**
 * Test MT provider services.
 */

require_once './class-abstracttranslationservice.php';
require_once './class-customproviderservice.php';
require_once './class-etranslationservice.php';

use WebtMtApiIntegrator\CustomProviderService;
use WebtMtApiIntegrator\EtranslationService;


/*
 *****************
 * DEFINE INPUTS *
 *****************
 */


/**
 * Define translatable strings.
 */
$translatable_strings = array( 'translatable string', 'one two three' );
$src_langcode         = 'en_GB';
$trg_langcode         = 'lv';

/**
 *  Define Custom provider data
 */
$custom_provider_url     = 'https://example.com/api';
$custom_provider_api_key = '00abc36b-7459-4e8b-ac86-b95ce1bd0c44';

/**
 * Define eTranslation data.
 */
$etranslation_appname               = 'MyEtranslationApplicationName';
$etranslation_password              = 'eTr@nslati0nPa$$worD';
$etranslation_response_endpoint_url = 'https://example.com/etranslation-response';
/**
 * Function that waits for eTranslation response body, retrieves and returns it.
 * Note: You should provide a separate HTTP endpoint handler function that saves eTranslation response somewhere, so that this function can find and access it.
 * For example, handler saves response into database, and this function periodically reads the database looking for response with given ID.
 *
 * This test function waits for 3s, then returns a static response.
 *
 * @param string $id Request ID.
 * @param string $to Target langcode.
 * @return string
 */
$etranslation_response_await_fn = function ( $id, $to ) {
	usleep( 3000000 );
	return EtranslationService::encode_request_body( array( 'tulkojama virkne', 'viens divi trīs' ) );
};





/*
 ***************************************************
 * TEST TRANSLATE AND LANGUAGE DIRECTION FUNCTIONS *
 ***************************************************
 */


/**
 *  Test Custom provider. Echoes translations and available systems.
 */
$custom_provider = new CustomProviderService( $custom_provider_url, $custom_provider_api_key );
// Translate strings.
$translations = $custom_provider->translate( $src_langcode, $trg_langcode, $translatable_strings );
print_r( $translations );
// Retrieve systems.
$systems = $custom_provider->get_supported_engine_list();
print_r( array_slice( $systems->languageDirections, 0, 5 ) ); //phpcs:ignore

/**
 * Test eTranslation provider. Echoes translations and available systems.
 */
$etranslation_provider = new EtranslationService(
	$etranslation_appname,
	$etranslation_password,
	$etranslation_response_endpoint_url,
	$etranslation_response_await_fn
);
// Translate strings.
$translations2 = $etranslation_provider->translate( $src_langcode, $trg_langcode, $translatable_strings );
print_r( $translations2 );
// Retrieve systems.
$systems2 = $etranslation_provider->get_supported_engine_list();
print_r( array_slice( $systems2->languageDirections, 0, 5 ) ); //phpcs:ignore
