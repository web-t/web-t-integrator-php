# WEB-T MT API Integrator PHP library

PHP library that contains both MT provider implementations for WEB-T solutions - eTranslation and Custom provider (Generic MT API) - and test examples of their usage.

## How to use
1. Test this library by providing your MT provider data in `test.php` file:
1.1. Custom provider API URL & API key 
1.2. eTranslation Application name & password, HTTP response endpoint URL and response await function (see function documentation in code). 
Run: `php test.php` from console. This will translate strings and retrieve available MT systems, then print results.
2. Integrate MT API Integrator into your website translation plugin.

## License
This library is licensed under Apache 2.0 license.
