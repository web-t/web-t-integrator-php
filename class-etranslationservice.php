<?php
/**
 * EtranslationService
 *
 * @package WebtMtApiIntegrator
 */

namespace WebtMtApiIntegrator;

/**
 * Etranslation MT engine integration.
 */
class EtranslationService extends AbstractTranslationService {

	/**
	 * Error response code dictionary.
	 *
	 * @var array
	 */
	private static $error_map = array(
		-20000 => 'Source language not specified',
		-20001 => 'Invalid source language',
		-20002 => 'Target language(s) not specified',
		-20003 => 'Invalid target language(s)',
		-20004 => 'DEPRECATED',
		-20005 => 'Caller information not specified',
		-20006 => 'Missing application name',
		-20007 => 'Application not authorized to access the service',
		-20008 => 'Bad format for ftp address',
		-20009 => 'Bad format for sftp address',
		-20010 => 'Bad format for http address',
		-20011 => 'Bad format for email address',
		-20012 => 'Translation request must be text type, document path type or document base64 type and not several at a time',
		-20013 => 'Language pair not supported by the domain',
		-20014 => 'Username parameter not specified',
		-20015 => 'Extension invalid compared to the MIME type',
		-20016 => 'DEPRECATED',
		-20017 => 'Username parameter too long',
		-20018 => 'Invalid output format',
		-20019 => 'Institution parameter too long',
		-20020 => 'Department number too long',
		-20021 => 'Text to translate too long',
		-20022 => 'Too many FTP destinations',
		-20023 => 'Too many SFTP destinations',
		-20024 => 'Too many HTTP destinations',
		-20025 => 'Missing destination',
		-20026 => 'Bad requester callback protocol',
		-20027 => 'Bad error callback protocol',
		-20028 => 'Concurrency quota exceeded',
		-20029 => 'Document format not supported',
		-20030 => 'Text to translate is empty',
		-20031 => 'Missing text or document to translate',
		-20032 => 'Email address too long',
		-20033 => 'Cannot read stream',
		-20034 => 'Output format not supported',
		-20035 => 'Email destination tag is missing or empty',
		-20036 => 'HTTP destination tag is missing or empty',
		-20037 => 'FTP destination tag is missing or empty',
		-20038 => 'SFTP destination tag is missing or empty',
		-20039 => 'Document to translate tag is missing or empty',
		-20040 => 'Format tag is missing or empty',
		-20041 => 'The content is missing or empty',
		-20042 => 'Source language defined in TMX file differs from request',
		-20043 => 'Source language defined in XLIFF file differs from request',
		-20044 => 'Output format is not available when quality estimate is requested. It should be blank or \'xslx\'',
		-20045 => 'Quality estimate is not available for text snippet',
		-20046 => 'Document too big (>20Mb)',
		-20047 => 'Quality estimation not available',
		-40010 => 'Too many segments to translate',
		-80004 => 'Cannot store notification file at specified FTP address',
		-80005 => 'Cannot store notification file at specified SFTP address',
		-80006 => 'Cannot store translated file at specified FTP address',
		-80007 => 'Cannot store translated file at specified SFTP address',
		-90000 => 'Cannot connect to FTP',
		-90001 => 'Cannot retrieve file at specified FTP address',
		-90002 => 'File not found at specified address on FTP',
		-90007 => 'Malformed FTP address',
		-90012 => 'Cannot retrieve file content on SFTP',
		-90013 => 'Cannot connect to SFTP',
		-90014 => 'Cannot store file at specified FTP address',
		-90015 => 'Cannot retrieve file content on SFTP',
		-90016 => 'Cannot retrieve file at specified SFTP address',
	);

	/**
	 * Service API URL.
	 *
	 * @var string
	 */
	private static $api_url = 'https://webgate.ec.europa.eu/etranslation/si';
	/**
	 * HTML tag part between translations.
	 *
	 * @var string
	 */
	private static $delimiter = '<param name="webt-delimiter" />';
	/**
	 * HTML tag which precedes every string translation
	 *
	 * @var string
	 */
	private static $prefix = '<html>';
	/**
	 * HTML tag which closes every string translation
	 *
	 * @var string
	 */
	private static $suffix = '</html>';

	/**
	 * Char limit which determines when to split translatable strings into separate request
	 *
	 * @var integer
	 */
	protected $max_chars_per_request = 60000;

	/**
	 * Application name for eTranslation API
	 *
	 * @var string
	 */
	private $application_name;
	/**
	 * Password for eTranslation API
	 *
	 * @var string
	 */
	private $password;
	/**
	 * HTTP endpoint URL which will receive eTranslation response
	 *
	 * @var string
	 */
	private $endpoint_url;
	/**
	 * Callback function which waits for async eTranslation response.
	 *
	 * @var function
	 */
	private $await_response_callback;

	/**
	 * Constructor for EtranslationService
	 *
	 * @param string   $application_name Application name for eTranslation API.
	 * @param string   $password Password for eTranslation API.
	 * @param string   $endpoint_url HTTP endpoint URL which will receive eTranslation response.
	 * @param function $await_response_callback Callback function which waits for async eTranslation response.
	 */
	public function __construct( $application_name, $password, $endpoint_url, $await_response_callback ) {
		$this->application_name        = $application_name;
		$this->password                = $password;
		$this->endpoint_url            = $endpoint_url;
		$this->await_response_callback = $await_response_callback;
	}

	/**
	 * Translates string array from source to target language using eTranslation.
	 *
	 * @param string $from langcode of source language.
	 * @param string $to langcode of target language.
	 * @param array  $values array of strings to be translated.
	 *
	 * @throws \Exception If credentials are not configured.
	 * @return array
	 */
	public function send_translation_request( $from, $to, $values ) {

		if ( ! $this->application_name || ! $this->password ) {
			throw new \Exception( 'eTranslation credentials not configured!' );
		}

		$lang_from = explode( '_', $from )[0];
		$lang_to   = explode( '_', $to )[0];

		// prepare translation request.
		$id      = uniqid();
		$content = self::encode_request_body( $values );

		if ( strlen( $content ) === 0 ) {
			return array();
		}

		$post   = $this->get_post_body( $id, $lang_from, $lang_to, $content );
		$client = $this->get_curl_client( '/translate' );

		curl_setopt( $client, CURLOPT_POST, 1 );
		curl_setopt( $client, CURLOPT_POSTFIELDS, $post );
		curl_setopt(
			$client,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen( $post ),
			)
		);

		// send translation request.
		$response    = curl_exec( $client );
		$http_status = curl_getinfo( $client, CURLINFO_RESPONSE_CODE );
		curl_close( $client );

		// check request response.
		$body       = json_decode( $response );
		$request_id = is_numeric( $body ) ? (int) $body : null;
		if ( 200 !== $http_status || $request_id < 0 ) {
			$message = self::$error_map[ $request_id ] ?? $body;
			$err     = curl_error( $client );
			syslog( LOG_ERR, "Invalid request response from eTranslation: $response [status: $http_status, message: $message, error: $err]" );
			return array();
		}

		syslog( LOG_DEBUG, "eTranslation request successful ($request_id) [ID=$id]" );

		// wait for translation callback.
		$callback = $this->await_response_callback;
		$response = $callback( $id, $to );
		if ( $response ) {
			return self::decode_response( $response );
		} else {
			return array();
		}
	}

	/**
	 * Initialize cURL client with URL
	 *
	 * @param string $url_part URL part following base API URL.
	 * @return \CurlHandle
	 */
	private function get_curl_client( $url_part ) {
		$application_name = $this->application_name;
		$password         = $this->password;

		$client = curl_init( self::$api_url . $url_part );
		curl_setopt( $client, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $client, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST );
		curl_setopt( $client, CURLOPT_USERPWD, $application_name . ':' . $password );
		curl_setopt( $client, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $client, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $client, CURLOPT_TIMEOUT, $this->request_timeout );

		return $client;
	}

	/**
	 * Generate JSON string for POST request body
	 *
	 * @param string $id Request ID.
	 * @param string $lang_from Source language code.
	 * @param string $lang_to Target language code.
	 * @param string $translatable_string base64 string containing translatable HTML.
	 * @return string
	 */
	private function get_post_body( $id, $lang_from, $lang_to, $translatable_string ) {
		$document = array(
			'content'  => $translatable_string,
			'format'   => 'html',
			'filename' => 'translateMe',
		);

		$translation_request_body = array(
			'documentToTranslateBase64' => $document,
			'sourceLanguage'            => strtoupper( $lang_from ),
			'targetLanguages'           => array(
				strtoupper( $lang_to ),
			),
			'errorCallback'             => $this->endpoint_url,
			'callerInformation'         => array(
				'application' => $this->application_name,
			),
			'destinations'              => array(
				'httpDestinations' => array(
					$this->endpoint_url,
				),
			),
			'externalReference'         => $id,
		);
		return json_encode( $translation_request_body );
	}

	/**
	 * Retrieves supported language directions and MT systems.
	 *
	 * @return array
	 */
	protected function send_language_direction_request() {
		$client      = $this->get_curl_client( '/get-domains' );
		$response    = curl_exec( $client );
		$http_status = curl_getinfo( $client, CURLINFO_RESPONSE_CODE );
		curl_close( $client );

		if ( 200 !== $http_status ) {
			syslog( LOG_ERR, "Error retrieving domains from eTranslation: $response [status: $http_status]" );
		}

		return array(
			'response' => $http_status,
			'body'     => json_decode( $response ),
		);
	}

	/**
	 * Map language direction response to Generic API response
	 *
	 * @param \stdClass $res Language direction response from MT provider.
	 * @return \stdClass
	 */
	protected function map_language_direction_response( $res ) {
		$systems = array();
		foreach ( $res as $domain => $value ) {
			$lang_pairs = $value->languagePairs; //phpcs:ignore
			foreach ( $lang_pairs as $pair ) {
				$lower = strtolower( $pair );
				$langs = explode( '-', $lower );

				$system          = new \stdClass();
				$system->srcLang = $langs[0]; //phpcs:ignore
				$system->trgLang = $langs[1]; //phpcs:ignore
				$system->domain  = $domain;
				$system->name    = $value->name;
				$systems[]       = $system;
			}
		}
		$response                     = new \stdClass();
		$response->languageDirections = $systems; //phpcs:ignore
		return $response;
	}

	/**
	 * Converts translatable string array to base64 encoded HTML string
	 *
	 * @param array $values Translatable strings.
	 * @return string
	 */
	public static function encode_request_body( $values ) {
		$str  = implode( self::$delimiter, $values );
		$html = self::$prefix . $str . self::$suffix;
		return base64_encode( $html );
	}

	/**
	 * Converts base64 encoded HTML string to translation string array
	 *
	 * @param string $base64html Translatable strings.
	 * @return array
	 */
	private static function decode_response( $base64html ) {
		$html = base64_decode( $base64html );
		$str  = substr( $html, strlen( self::$prefix ), strlen( $html ) - strlen( self::$prefix ) - strlen( self::$suffix ) );
		return explode( self::$delimiter, $str );
	}
}
