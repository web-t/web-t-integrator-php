<?php
/**
 * CustomProviderService
 *
 * @package WebtMtApiIntegrator
 */

namespace WebtMtApiIntegrator;

/**
 * Custom provider MT engine integration.
 */
class CustomProviderService extends AbstractTranslationService {

	/**
	 * Custom provider API Base URL
	 *
	 * @var string
	 */
	protected $url;

	/**
	 * Custom provider API key
	 *
	 * @var string
	 */
	protected $api_key;

	/**
	 * Constructor for CustomProviderService
	 *
	 * @param string $url Custom provider API Base URL.
	 * @param string $api_key Custom provider API key.
	 */
	public function __construct( $url, $api_key ) {
		$this->url     = $url;
		$this->api_key = $api_key;
	}

	/**
	 * Translates string array from source to target language using Custom provider.
	 *
	 * @param string $from langcode of source language.
	 * @param string $to langcode of target language.
	 * @param array  $values array of strings to be translated.
	 *
	 * @throws \Exception If credentials are not configured.
	 * @return array
	 */
	protected function send_translation_request( $from, $to, $values ) {
		$data          = (object) array();
		$data->srcLang = explode( '_', $from )[0]; //phpcs:ignore
		$data->trgLang = explode( '_', $to )[0]; //phpcs:ignore
		$data->text    = $values;

		if ( ! $this->url || ! $this->api_key ) {
			throw new \Exception( 'Translation provider not configured!' );
		}

		$body    = json_encode( $data, JSON_UNESCAPED_SLASHES );
		$headers = array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen( $body ),
			'Accept: application/json',
			"X-API-KEY: $this->api_key",
		);

		$client = $this->get_curl_client( '/translate/text', $headers );

		curl_setopt( $client, CURLOPT_POST, 1 );
		curl_setopt( $client, CURLOPT_POSTFIELDS, $body );

		$response    = curl_exec( $client );
		$http_status = curl_getinfo( $client, CURLINFO_RESPONSE_CODE );
		curl_close( $client );

		if ( 404 === $http_status ) {
			syslog( LOG_ERR, 'Translation endpoint not found!' );
			return array();
		}
		if ( 200 !== $http_status ) {
			syslog( LOG_ERR, 'Invalid response status code from translation provider: ' . $http_status );
			return array();
		}

		$json = json_decode( $response );

		$translations = array_map(
			function( $item ) {
				return $item->translation;
			},
			$json->translations
		);
		return $translations;
	}

	/**
	 * Retrieves supported language directions and MT systems.
	 *
	 * @throws \Exception If provider data is not configured.
	 * @return mixed
	 */
	protected function send_language_direction_request() {
		if ( ! $this->url || ! $this->api_key ) {
			throw new \Exception( 'Translation provider not configured!' );
		}

		$headers = array(
			'Accept: application/json',
			"X-API-KEY: $this->api_key",
		);

		$client      = $this->get_curl_client( '/translate/language-directions', $headers );
		$response    = curl_exec( $client );
		$http_status = curl_getinfo( $client, CURLINFO_RESPONSE_CODE );
		curl_close( $client );

		if ( 200 !== $http_status ) {
			syslog( LOG_ERR, "Error retrieving language directions: $response [status: $http_status]" );
		}

		return array(
			'response' => $http_status,
			'body'     => json_decode( $response ),
		);
	}

	/**
	 * Initialize cURL client with URL
	 *
	 * @param string $url_part URL part following base API URL.
	 * @param array  $headers HTTP request headers.
	 * @return \CurlHandle
	 */
	private function get_curl_client( $url_part, $headers ) {
		$client = curl_init( $this->url . $url_part );

		curl_setopt( $client, CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $client, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $client, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $client, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $client, CURLOPT_TIMEOUT, $this->request_timeout );

		return $client;
	}
}
