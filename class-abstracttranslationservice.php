<?php
/**
 * AbstractTranslationService
 *
 * @package WebtMtApiIntegrator
 */

namespace WebtMtApiIntegrator;

/**
 * Abstract translation provider engine
 */
abstract class AbstractTranslationService {

	/**
	 * Char limit which determines when to split translatable strings into separate request
	 *
	 * @var integer
	 */
	protected $max_chars_per_request = 60000;

	/**
	 * HTTP client request timeout.
	 *
	 * @var integer
	 */
	protected $request_timeout = 600;

	/**
	 * Translates string array from source to target language.
	 *
	 * @param string $from langcode of source language.
	 * @param string $to langcode of target language.
	 * @param array  $values array of strings to be translated.
	 *
	 * @throws \Exception If credentials are not configured.
	 * @return array
	 */
	abstract protected function send_translation_request( $from, $to, $values );

	/**
	 * Retrieves supported language directions.
	 *
	 * @return mixed
	 */
	abstract protected function send_language_direction_request();

	/**
	 * Translates string array from source to target language using one or more HTTP requests.
	 *
	 * @param string $from langcode of source language.
	 * @param string $to langcode of target language.
	 * @param array  $values array of strings to be translated.
	 * @return array
	 */
	public function translate( $from, $to, $values ) {

		$current_char_len    = 0;
		$request_value_lists = array();
		$current_list        = array();

		// split into multiple arrays not exceeding max char limit.
		foreach ( $values as $value ) {
			$chars = strlen( $value );
			if ( $current_char_len + $chars > $this->max_chars_per_request ) {
				$request_value_lists[] = $current_list;
				$current_list          = array( $value );
				$current_char_len      = $chars;
			} else {
				$current_list[]    = $value;
				$current_char_len += $chars;
			}
		}
		if ( ! empty( $current_list ) ) {
			$request_value_lists[] = $current_list;
		}

		// send requests and merge results together.
		$full_result   = array();
		$request_count = count( $request_value_lists );

		for ( $i = 0; $i < $request_count; $i++ ) {

			$contains_only_empty_strings = empty(
				array_filter(
					$request_value_lists[ $i ],
					function ( $a ) {
						return ! empty( $a );
					}
				)
			);

			if ( $contains_only_empty_strings ) {
				$full_result = array_merge( $full_result, $request_value_lists[ $i ] );
			} else {
				syslog( LOG_DEBUG, "Sending $from->$to translation request " . $i + 1 . '/' . $request_count . ' (' . count( $request_value_lists[ $i ] ) . ' strings)' );
				$translations = $this->send_translation_request( $from, $to, $request_value_lists[ $i ] );

				if ( ! $translations || empty( $translations ) ) {
					$retries_left = 2;
					while ( $retries_left > 0 ) {
						syslog( LOG_WARNING, "Translation request failed, retrying... (retries left: $retries_left)" );
						$translations = $this->send_translation_request( $from, $to, $request_value_lists[ $i ] );
						$retries_left--;
					}

					// do not continue if one of requests fail.
					if ( ! $translations || empty( $translations ) ) {
						if ( $request_count > 1 ) {
							syslog( LOG_ERR, 'One of translation requests failed' );
						}
						return array();
					}
				}
				// merge with previous translations.
				$full_result = array_merge( $full_result, $translations );
			}
		}
		return $full_result;
	}

	/**
	 * Map language direction response to Generic API response
	 *
	 * @param \stdClass $response Language direction response from MT provider.
	 * @return \stdClass
	 */
	protected function map_language_direction_response( $response ) {
		return $response;
	}

	/**
	 * Retrieves supported MT system list.
	 *
	 * @return \stdClass
	 */
	public function get_supported_engine_list() {
		$language_directions = $this->send_language_direction_request()['body'];
		return $this->map_language_direction_response( $language_directions );
	}
}
